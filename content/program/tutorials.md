+++
title = "Tutorials at useR! 2021"
description = "What's happening at the event"
keywords = ["program","schedule","events","topics"]
+++


Every year, useR! features a tutorial program addressing the diverse interests of its audience. R users/developers worldwide who wish to learn about new technologies or enhance their knowledge about the existing technologies and novices to the #Rstats world interested in introductory tutorials are welcome to participate. The tutorials will be for R users in all sectors, e.g., researchers, government officers, and industry representatives who focus on R's applicability in practical settings.

We strongly believe that similar tutorials have helped the R user community to be updated about the latest packages, concepts, and best practices.

Since we have a global event this year, we will have a full day of tutorials in different timezones. Tutorials will be taught in English, Spanish and French.

Tutorial submission is open now. **<a href="https://user2021.r-project.org/participation/call-for-tutorials/">Submit a tutorial now!</a>**

<br><br><br>

