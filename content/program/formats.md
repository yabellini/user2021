+++
title = "Formats description"
description = "What different conference formats are proposed"
keywords = ["program","schedule","events","topics"]
+++

useR! 2021 will be virtual and a very different experience for all the participants from all around the world! This page gives you an overview and short description of the formats in our <a href="https://user2021.r-project.org/program/overview">Overview Schedule</a> so that you know what to expect.

## Regular talks
As for every useR!, regular talks will also be the backbone of useR! 2021, with high-quality contributions from different fields. The talks will be limited to 15 minutes followed by a short Q&A time-slot. Thanks to the online format of this years conference, talks can be either live or pre-recorded. However, the Q&A part will be live to encourage engagement of the conference participants. One session of regular taks will contain 4-5 talks and there will be several sessions in parallel. The virtual format of this year's conference also allows for talks being in the presenters mother tongue, as the talks can easily be accompanied by subtitles (the speakers only have to provide an English transcript).


## Elevator pitches
One of useR!2021's new formats are the _Elevator pitches_. They replace the regulare poster session and can be presented in two different forms: a technical note or a lightning talk. Lightning talks are 5 minutes long. Technical notes should contain some text and graphs - like a poster - be short and readable in 5 minutes. This is where any work related to R that may not quite fit in the regular talk category is fair game. Elevator pitch sessions will be scheduled throughout the conference and designated Q&A sessions will be hosted to allow for direct interactions with the presenters. Also the elevator pitch contributions can be in a language different from English as long as the speaker provides an English transcript or translation.

## Panels and Incubators
Panels are suitable for topics that are of interest to a large audience. The idea behind the panels is to engage in a 60-minute topic discussion with experts/stakeholders. Interaction with the audience (e.g., a moderated Q&A) is possible. Incubators, on the other hand, address specific topics that are of interest to a smaller group of people working in the same field. They have a specific aim (i.e., formulate ten principles for good XX) and are not mere networking sessions. 

<br><br><br>
