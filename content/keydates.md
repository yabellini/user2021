+++
title = "Key Dates"
description = "Important Deadlines for useR! 2021. The annual user conference of the R Language for Statistical Computing."
keywords = ["program","schedule","events","topics"]
+++


# Roadmap to useR! 2021

The below list contains key deadlines for useR! 2021 participants. 

**2020-12-15** Tutorial submission opens.

**2021-01-25** Abstract submission for regular talks, panels, incubators and elevator pitches opens.

**2021-02-05** Tutorial submission closes.

**2021-03-22** Abstract submission closes

**2021-03-12** Communicate selected tutorials.

**2021-04-15** Registration opens.

**2021-05-10** Communicate selected abstracts.

**2021-05-15** Early Bird registration closes.

**2021-06-20** Deadline to send us videos, blogposts and scripts.

**2021-07-05** Conference opens.

**2021-07-09** Conference ends.


Stay tuned for the conference schedule and program information. It will be out soon!

<br>
<br>
<br>
<br>
