+++
title = "Code of Conduct"
description = "Expected behavior of conference participants. Respect boundaries."
keywords = ["Code of Conduct","CoC"]
+++


The organizers of useR! 2021 are dedicated to providing a safe and inclusive conference experience for everyone regardless of age, gender, sexual orientation, disability, physical appearance, race, or religion (or lack thereof).

All participants (including attendees, speakers, sponsors and volunteers) at useR! 2021 are required to agree to the following code of conduct.

The code of conduct applies to all conference activities including talks, panels, workshops, and social events. It extends to conference-specific exchanges on social media, for instance posts tagged with the identifier of the conference (e.g. #useR2021 on Twitter), replies to such posts, and replies to official social media accounts (e.g. [@_useRconf](https://twitter.com/_useRconf) on Twitter).

Organizers will enforce this code throughout and expect cooperation in ensuring a safe environment for all.

## Expected Behavior

All conference participants agree to:

  + Be considerate in language and actions, and respect the boundaries of fellow participants.
  + Refrain from demeaning, discriminatory, or harassing behavior and language. Please refer to [__Unacceptable Behavior__](#unacceptable-behavior) for more details.
  + Alert a member of the [__Code Of Conduct Response Team__](#code-of-conduct-response-team)  if you notice someone in distress, or observe violations of this code of conduct, even if they seem inconsequential. Please refer to the section titled [__What To Do If You Witness or Are Subject To Unacceptable Behavior__](#what-to-do-if-you-witness-or-are-subject-to-unacceptable-behavior) for more details.

## Unacceptable Behavior

Behavior that is unacceptable includes, but is not limited to:

  + Stalking
  + Deliberate intimidation
  + Unwanted photography or recording
  + Sustained or willful disruption of talks or other events
  + Use of sexual or discriminatory imagery, comments, or jokes
  + Offensive comments related to age, gender, sexual orientation, disability, race or religion
  + Inappropriate physical contact, which can include grabbing, or massaging or hugging without consent
  + Unwelcome sexual attention, which can include inappropriate questions of a sexual nature, asking for sexual favors or repeatedly asking for dates or contact information

If you are asked to stop unacceptable behavior you should stop immediately, even if your behavior was meant to be friendly or a joke, it was clearly not taken that way and for the comfort of all conference attendees you should stop.

Attendees who behave in a manner deemed inappropriate are subject to actions listed under [__Procedure for Code of Conduct Violations__](#procedure-for-code-of-conduct-violations).

#### Additional Requirements for Conference Contributions

Presentations should not contain offensive or sexualised material. If this material is impossible to avoid given the topic (for example text mining of material from hate sites) the existence of this material should be noted in the abstract and, in the case of oral contributions, at the start of the talk or session.

#### Additional Requirements for Sponsors

Sponsors should not use sexualised images, activities, or other material. Booth staff (including volunteers) should not use sexualised clothing/uniforms/costumes, or otherwise create a sexualised environment. In case of violations, sanctions may be applied without return of sponsorship contribution.

## Procedure for Code of Conduct Violations

The organizing committee reserves the right to determine the appropriate response for all code of conduct violations. Potential responses include:

+ a formal warning to stop harassing behavior
+ expulsion from the conference
+ removal of sponsor displays or conference presentations
+ cancellation or early termination of talks or other contributions to the program

Refunds may not be given in case of expulsion.

## What To Do If You Witness or Are Subject To Unacceptable Behavior

Alert the **Code Of Conduct Response Team** if you notice someone in distress, or observe violations of this code of conduct, even if they seem inconsequential.

Reports should be addressed to **[useR2021-coc [at] r-project.org](mailto:useR2021-coc@r-project.org)**.
The report is shared with all members of the [__Code Of Conduct Response Team__](#code-of-conduct-response-team). Correspondence is handled confidentially and deleted after the case was resolved.
If the report concerns someone from the response team, you are welcome to contact the R-Foundation Conference Committee under [R-conferences [at] r-project.org](mailto:r-conferences@r-project.org).

We will take all good-faith reports by useR! 2021 participants seriously. This includes incidents outside our spaces and at any point in time. The useR! 2021 organizing committee reserves the right to exclude people from useR! 2021 based on their past behavior, including behavior outside useR! 2021 spaces and behavior towards people who are not part of useR! 2021.

We reserve the right to reject any report we believe to have been made in bad faith. This includes reports intended to silence legitimate criticism.

We will respect confidentiality requests for the purpose of protecting victims of abuse. We will not name victims without their affirmative consent. All data is stored securely, access will be limited to the __Code Of Conduct Response Team__.

Conference staff will also provide support to victims, including, but not limited to:

+ Briefing key event staff to provide appropriate assistance
+ Other assistance to ensure victims that they feel safe for the duration of the conference.

Assistance will only be provided with affirmative consent of the victim. 

## Code Of Conduct Response Team

Gordana Popovic  
Natalia Morandeira  
Andrea Sánchez-Tapia


## Acknowledgements

Parts of the above text are licensed CC BY-SA 4.0. Credit to SRCCON. This code of conduct was based on that developed for useR! 2018 which was a revision of the code of conduct used at previous useR!s and also drew from rOpenSci’s code of conduct.

<br><br><br>