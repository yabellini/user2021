+++
title = "Accessibility Standards for useR! 2021 talks"
description = "Guidelines and standards to make talks accessible."
keywords = ["Tutorials","Accessibility"]
+++

# UseR! 2021 Accessibility Standards for Regular Talks and Lightning Talks

For all talks (regular talks and lightning presentations) we kindly ask that prospective presenters: 

+ Prefer text-based slides platforms such as markdown or Beamer
+ Avoid transitions, animations, and complicated layouts
+ Add alt-text to images that explain completely the features in the image 
+ If you still need to present in MS PowerPoint or similar, keep the original file
+ Add speaker notes to your slides
+ Make the slides available in an accessible formats before your talk.
+ If you are giving a talk in a language different from English, provide the English transcript beforehand.
+ The slides and other materials need to be in English.

## Other reminders

+ Speak as clearly as possible, looking at the camera, and try not to go too fast.
+ Mention slide headers or numbers when switching slides.
+ During the session, comment briefly on what it is that you are showing and why. Avoid explanations that rely only on obvious features that may not be obvious for everyone, such as "As you can see" or "The image speaks for itself"

For more information, check the useR!2021 [Accessibility Guidelines](/participation/accessibility) or contact [user2021-accessibility[at]r-project.org](mailto:user2021-accessibility@r-project.org)
