+++
title = "Accessibility Standards for useR! 2021 Panels and Incubators"
description = "Guidelines and standards to make panels and incubators accessible."
keywords = ["Tutorials","Accessibility"]
+++

# UseR! 2021 accessibility standards for Panels and Incubators

For Panels and Incubators, we kindly ask that prospective organisers of a panel/incubator session: 

+ Please refer to our guidelines for slides if you plan to show slides in your panel or incubator. 
+ If the incubator's goal is to generate an output, please ensure that the output is accessible too. 
+ Get in touch with the accessiblity team well in advance of the conference if you have doubts about the accessibility of any tool that you plan to use during the panel or incubator. 
+ Instruct the participants of your panel to speak clearly and not too fast.

## Other reminders

- Speak as clearly as possible, looking at the camera, and try not to go too fast.
- Be aware of clues in the chat that signal if your pace is letting everyone follow your instructions 
- During the session, comment briefly on what it is that you are showing and why. Avoid explanations that rely only on obvious features that may not be obvious for everyone, such as "As you can see" or "The image speaks for itself"

For more information, check the useR!2021 [Accessibility Standards](/participation/accessibility) or contact [user2021-accessibility[at]r-project.org](mailto:user2021-accessibility@r-project.org)


