+++
title = "It Doesn't Just Happen"
date = "2020-11-19"
tags = ["Community"]
author = "Matt Bannert and Miljenka Vuko"
categories = ["Community"]

+++


“Not very probable,” is what we would have said only a few years ago when asked as statisticians, about the chances that our working environment, our work and peers equate to a diverse, inclusive and remarkably social community. Still though, all of this continues to happen within a discipline that is built on focused, cerebral work and spending countless hours in front of computer screens.

# A Journey Through a Wonderful Community Ecosystem

_We thank community leaders Laura Acion, Shelmith Kariuki, Yanina Bellini Saibene,
Heather Turner and GSoC scholar Ben Ubah for their input and time as well as Beatriz Milz for her help 
with our research._


As a group of conference organizers we learned about the true reach of the R Language's
community ecosystem implicitly while coming together from all parts of the world. Before joining forces to work on useR! 2021, to many of us being an R enthusiast had felt like being at the outer rim of something that pulsates: Most of us recognized shockwaves like the release of R 4.0 but other than that it was hard to anticipate how vibrant the R community actually felt once we came closer to the center. 

Or put more accurately, to one of its epicenters. And yet, this rather spatial imagination of multiple centers sending out overlapping waves of energy falls short of truly grasping the complexity of the community ecosystem. Centered around the R Language for Statistical Computing, local groups, special interest groups, and stewards appear to find their role in nurturing a wonderful, global community. 

## The Beginning

Our story starts with Heather Turner. When we decided to do a piece on R's community ecosystem, Heather was the natural starting point for us. Not only because we were lucky enough she became the link to the R Foundation for our group of useR! organizers. She is what one would call a true community root. 
Heather has been around the community and conference before many of us even graduated (from high school). Today, she is the chair of [_Forwards_](https://forwards.github.io/about/), "which began as an R Foundation taskforce to address the under-representation of women in the R community. It started in 2015, following growing concern over the issue, highlighted in a panel at useR! 2014," she explains. In the process of setting their taskforce up, the R Foundation became more aware that under-representation did not stop at gender. "We felt it made sense to address diversity across the board. So we expanded our mission and rebranded as Forwards to signal this change", remembers Heather.

Members of Forwards include people from the R Foundation to be able to influence policy and practice. Forwards focuses on R Foundation supported activities such as useR!, CRAN, and the R project itself, but also works on its own initiatives. They can be found supporting the development of community groups, helping useR! organizers to make the conference inclusive, or developing teaching materials shared on their [Education site](http://forwards.github.io/edu/). "We only run a few events directly and these have all been in English, so far," Heather told us, pointing us in the next direction of our journey. Forwards has helped the local conferences ConectaR and LatinR get started, and provides support to AfricaR, but they also leave these local communities plenty of room to grow.
## Local Communities

And this room is being picked up. One of the prime examples of a well established and grown local community is LatinR, a conference that became possibly the strongest non-English speaking community of the R World. Started in only 2017, the roots of the Latin American R community lead to another community group: [R-Ladies](https://rladies.org/).

R-Ladies [Yanina Bellini Saibene](https://twitter.com/yabellini) and [Laura Ación](https://twitter.com/_lacion_?) started small by organizing a regular call in a Spanish speaking R-Ladies channel and hit big by initializing a truly special local community. "We collectively help each other reach further, even in the face of scarce resources that make us compete against each other, "
said Laura when we asked her to explain what's special about the LatinR community. "I think that is because when one person of Latin America succeeds at something, you will find many public demonstrations of happiness. In a way, we all feel represented by folks that succeed and are from our region."

<div class="catchy">
    <div class="catchy-big">&nbsp;"Act locally,<br> &nbsp; think globally."</div>
    <div class="catchy-small">
        -- Laura Acion, LatinR.
    </div>
</div>

"Part of having scarce resources in the region includes very limited resources to learn a second (natural) language on top of learning R. Thus, having a conference in Spanish and Portuguese, the two main languages spoken in Latin America is a must for us, " said Laura. "In Latin America, if people can afford to speak a second language, it is often English rather than Spanish or Portuguese." Obviously, English not only bridges gaps within Latin America but is also the connection to the global community. "Act locally, think globally, " is how LatinR summed up their approach.

While the support of _Forwards_ and their calm and patient let-them-grow approach played its role in how these major communities around the world panned out, the common ground of the larger regional community groups is their ability to motivate truly special, dedicated, and enduring regional leaders. Just like Laura and Yanina, [Shelmith Kariuki](https://twitter.com/shel_kariuki) is another prime example of such an unique person. One of the great things about talking to her is how she transfers her stirring African spirit and motivation even in an online meeting. 

Given the sheer size and heterogeneity of the continent, [AfricaR](https://africa-r.org/) needed to make sure their leadership was spread. "Like recently someone from Mwanza, Tanzania reached out to me asking how I or we could help them start an R user group in their city," Shel explains her work in East Africa in practice. "We have people in the francophone areas of Africa who take care of ensuring we have meetups going on there, not only in English but also in French so that they are inclusive. We also have some friends in northern Africa who again ensure that we have a good R representation there."

Again, just like in South America, Miljenka and I learned that women and R-Ladies played an important role 
in connecting AfricaR to likeminded people, e.g., in Tunisia or #NairobiR.

<div class="imgpost">
   <img src="/img/blog/nairobiR.jpeg" width="450px">
   <div class="postcaption">Africa has many active R user groups like NairobiR from Kenya.</div>
</div>


The common goal of these different groups in Africa is to make sure no region is left out. "The leadership team is also spread out and I think that helps," Shel concluded. Back when she found out about the global R community on social media, met AfricaR co-founder Dennis Irorere from Nigeria and Timnit Gebru, now a technical co-lead of Ethical AI at Google, who pointed out to the tech world how Africans were denied visas to attend scientific conferences, things got going.

"We formed a larger team, because it was getting too heavy on Dennis and I," Shel remembered. Ahmadou [Dicko], based in Senegal, who **is now** also a global coordinator for the upcoming useR! 2021, was the first add-on to the leadership team. As of today, leadership has grown to more than a handful of people. And this a lesson learned that AfricaR leaders pass on to those who are starting local groups. "We have learned that leading a community needs a lot of dedication and that is what we advise people who approach us wanting to come up with communities in their cities. We help them but the one thing we always encourage them to do is find a partner who will lead with them, so when they are busy, the meetup or the group still runs as it should". 


<div class="catchy">
    <div class="catchy-big">&nbsp;"Patience."</div>
    <div class="catchy-small">
        -- Shelmith Kariuki, AfricaR.
    </div>
</div>


"Patience," said Shel when asked for another main lesson learned. And the endurance seems to pay off. "Before we had software like SPSS, but people are slowly appreciating the importance of R, especially when it comes to reproducibility. We have even received messages from people who are still in school inviting us to give talks in different campuses, which is good. We’ve also noticed more and more job descriptions that require people to know R, which was a bit different in 2018 / 2019 when we started."

Shel and the AfricaR leadership have plans to grow further. "We would love to have people from countries such as Somalia or Sudan, those countries that do not have representation yet, we would love to have that in the coming months." Hopefully, an inclusive, global useR! conference can help spread the R Language and community. 

## The Common Denominator

When the current useR! team came together from around the world a few months ago, we realized the group
had a lot more in common than a passion for statistical computing and funny puns with 18th letter of the alphabet. Somehow, those who stepped up out of their local communities to join our global team shared an  appreciation for an inclusive atmosphere as well as the awareness that a respectful community event does not just happen. The joint influence of patient community work from all the aforementioned groups becomes palpable in these moments.

Part of the secret may be that established groups allow for renewal and rather see new groups as contributors with new perspectives as opposed to competitors they could not influence. A few weeks into our research we met [Danielle Smalls](https://twittter.com/smallperks), who joined our Slack space. Danielle is a founder of a relatively new group. Together with [Dorris Scott](https://twitter.com/Dorris_Scott), she co-founded MiR -- [a community for underrepresented minority users of R](https://medium.com/@doritolay/introducing-mir-a-community-for-underrepresented-users-of-r-7560def7d861) during a breakout session at last year's RStudio::conf after they both had received a diversity scholarship. 

<div class="catchy">
    <div class="catchy-mid">&nbsp;"We hope to minimize the
    &nbsp;&nbsp;feeling of 'being the only 
    &nbsp;&nbsp;one' within the R 
    &nbsp;&nbsp;community at-large."</div>
    <div class="catchy-small">
        -- Danielle Smalls, MiR.
    </div>
</div>


"We hope to minimize the feeling of _being the only one_ within the R community at-large," Danielle explained. As of today, the MiR Community has around 140 members and continues to grow. "The community was designed to support, uplift, and grow the careers of underrepresented R users through professional development and ally support.  Right now, most of our members are from communities that are traditionally marginalized in tech." R users who are women, R users who are non-binary, R users from historically marginalized races, R users who are trans, R users with disabilities and R users who are queer can currently join through a [Community Registration Form](https://docs.google.com/forms/d/e/1FAIpQLScDFSyMLh_BbEq7zEBR18obMz8E2jQ6ZQYg4UPUxrfP93M46Q/viewform).

## Beyond Local

Another important building block of the community are success stories new projects look up to. R-Ladies is such a community success. At 193 chapters in 54 countries and more than 76000 members, R-Ladies does not need an introduction anymore.

"Then in 10 years we can stop R-Ladies because we are not needed anymore," said Yanina half-jokingly, when we asked her about the group's strategic plan. In other words the R-Ladies intends to make its voice heard to the degree that equality is so natural in the R world that it does not need a dedicated voice anymore. Until then, "we see an R-Ladies global community with a structure that contemplates the global reality, where all the voices of the different realities and communities that make up R-Ladies are heard and taken into account to shape our community.  We want to make an impact [...] that there is more gender diversity in package developers or within the R Core Team."

How does such a community juggernaut maneuver? How do the global leads work together with local chapters?
"Chapters are the heart of R-Ladies. The Global and Leadership team communicates with the organizers of each chapter through a Slack. The Global Team provides the infrastructure for each chapter as email, meetup, GitHub, and also are responsible for global initiatives like WeAreRLadies, the R-Ladies Directory, and the R-Ladies review network.  A lot of these global initiatives were ideas of chapters organizers, discussed in slack, and then implemented by R-Ladies Global which is composed of volunteers that were or are chapter organizers."


<div class="catchy">
    <div class="catchy-mid">And in case you were wondering, men are not excluded from R Ladies.</div>
</div>

"What is reserved for women and other gender minorities are leadership, coordinating, organizers, and speaking positions and the numbers of attendees should not all be of the majority gender. The idea is to provide a safe space so that we can develop our full potential as users and developers of R," explained Yanina.  

The umbrella and leadership team has been working to sort internal processes out and will soon announce a new definition of the group's governance as a community. "We believe that our structure and the way we run the organization reflect our values so we believe that we have to give ourselves the possibility to rethink how we want to function and grow as a global organization, and create a structure that better expresses our values and our commitment to R-ladies as a global community of communities."

Having learned so much about Forwards who are an R Foundation initiative, AfricaR, LatinR, and R-Ladies, which are organized as as umbrellas and meta communities, we invitably began to wonder how the plethora of local R, meetup-type of R user groups related to these bigger groups. Even the smaller MiR seemed to have a center. One gets the impression that the spirit, the conduct and the respectful exchange promoted by the aforementioned groups rubs off on the smaller, local meetups. 

## How Smaller R Meetups Perceive the Community

We brought in Ben Ubah from Nigeria who was awarded with a Google Summer of Code (GSoC) project for his proposal of a _Community Explorer_ with an "idea to aggregate the many activities around the global R community into a central place where new and existing users could easily navigate the community." Ben, who claimed he did not even work with R in his professional life, certainly embodies this community spirit. A few days after we dropped him an e-mail with an interview request about this work on the community explorer, he had joined our Slack space. Not only had he answered all our questions, but already got to work to contribute to our team of useR! organizers. 

"I think the R Community is amongst the pacesetters for inclusivity. What this means to me is that, you can easily reach out to the CTO of so-and-so big company that provides R services, and you would get a kind and welcome response that doesn't make you feel out of the picture. In fact some people within the R community could treat you like you were one of their relatives," said Ben. 

Encouraged by a warm welcome to the community Ben cooked up an early version of an aggregator back in 2015: "a central place where new and existing users could easily navigate the community and find their way around. r-central.com was the first thing I did, but was not satisfied," he remembered. Ben is not a person who gives up easily. After his proposal for a GSoC was rejected in 2018, he came back and in 2019 and was awarded his GSoC project mentored by [Claudia Vitolo](https://twitter.com/clavitolo), a co-founder of R-Ladies, and [Rick Pack](https://twitter.com/rick_pack2), a Lab Corp data scientist. 


<div class="imgpost">
   <a href="https://benubah.github.io/r-community-explorer/" target="_blank"><img src="/img/blog/explorer.png" width="450px"></a>
   <div class="postcaption">Click the image to give Ben's Community Explorer a try.</div>
</div>


Through the project he learned how to create [a modern static dashboard](https://benubah.github.io/r-community-explorer/) backed by serverless technology. 
His Community Explorer is updated daily and blends R with frontend technologies as well as state-of-the-art deployment using GitHub Pages and CI/CD. Thanks to its lean architecture the community explorer including Ben's favorite, interactive leaflet map can be hosted on GitHub without costs for him and the community. 

And he is not done yet. "Exploring [local user-groups](https://benubah.github.io/r-community-explorer/rugs.html), [R-Ladies chapters](https://benubah.github.io/r-community-explorer/-rladies.html) and [GSoC projects](https://benubah.github.io/r-community-explorer/gsoc.html) was the initial deposit. I am looking at the R ecosystem consisting of people (users), and their activities across Meetups, events (useR!, satRdays, etc), CRAN, Twitter, Stack Overflow, Search Engines, R-Bloggers, etc. You may want to look at our [useR! 2020 presentation on The State of R](https://www.youtube.com/watch?v=7F7iQdcV8RY)."

So what insights with respect to groups' awareness of each other and flow information can we take away from Ben's work? "Local R user groups form the fabric for a coherent and sustainable community. With easily accessible data from sources like Meetup.com, [Jumping Rivers user-group collection](https://jumpingrivers.github.io/meetingsR/r-user-groups.html), [R Consortium Meetup pro account](https://www.meetup.com/pro/r-user-groups/), and [R Community Explorer](https://github.com/benubah/r-community-explorer), it is reasonable to say that user-groups are well aware of each other. From my work, I have seen R user-groups holding joint meetings and moreso with groups from other tech communities".

On the way out, Ben had a word of advice that seems particularly relevant to us as organizers of useR! 2021. "One common denominator I see across local groups is the groups' organizer(s). The strength and activeness of every group would somehow depend on the organizers. I would suggest to recognize (celebrate) the efforts of several top organizers who put in a lot of volunteer time to keep user-groups active and growing during useR! 2021." 

<br>
<br>
<br>
<br>
